import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { Link } from "react-router-dom";

export default function DetailJob() {
  const params = useParams();
  const [data, setData] = useState([]);
  useEffect(() => {
    const fetchData = async () => {
      await fetch(
        `http://dev3.dansmultipro.co.id/api/recruitment/positions/${params.id}`
      )
        .then((response) => response.json())
        .then((actualData) => {
          setData(actualData);
        })
        .catch((err) => {
          console.log(err.message);
        });
    };
    fetchData();
  }, [params.id]);
  return (
    <>
      <section>
        <div>
          <Link to={`/joblist`}>
            <span className="label-text text-2xl font-bold text-blue-600 p-4 col-span-1 ">
              Back
            </span>
          </Link>
        </div>
      </section>
      <section
        style={{ height: "100vh", overflowY: "scroll" }}
        className="pt-5"
      >
        <div className="box-content w-full border-4">
          <div>
            <span className="pl-3">
              {data.type} / {data.location}
            </span>
            <h1 className="text-4xl font-bold pl-3 pt-3">{data.title}</h1>
          </div>
          <div className="divider p-3"></div>
          <div className="grid grid-cols-2 gap-1 pl-5 justify-items-center">
            <div dangerouslySetInnerHTML={{ __html: `${data.description}` }} />
            <div>
              <div className="card w-96 h-auto max-h-96 bg-base-100 shadow-xl">
                <figure>
                  <img src={data.company_logo} alt="Company Logo" />
                </figure>
                <div className="card-body">
                  <h2 className="card-title">{data.company}</h2>
                  <a href={data.company_url}>
                    <span className="text-blue-500">{data.company_url}</span>
                  </a>
                </div>
              </div>
              <div className="p-5"></div>
              <div className="card w-96 bg-base-100 shadow-xl">
                <div className="card-body">
                  <h2 className="card-title">How to pply</h2>
                  <p
                    dangerouslySetInnerHTML={{ __html: `${data.how_to_apply}` }}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}
