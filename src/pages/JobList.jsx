import { useEffect, useState, useRef } from "react";
import JobListComponent from "../components/JobListComponent";

export default function JobList() {
  const [data, setData] = useState([]);
  let [description, setDescription] = useState("");
  let [location, setLocation] = useState("");
  let [fullTime, setFullTIme] = useState(false);
  const [currPage, setCurrPage] = useState(1);
  const [prevPage, setPrevPage] = useState(0);
  const [lastList, setLastList] = useState(false);
  const [title, setTitle] = useState("Job List");
  const [countResult, setCountResult] = useState(0);
  let [param, setParam] = useState(`?description=&location=&full_time=false`);

  const listinnerref = useRef();

  const onMore = () => {
    if (listinnerref.current) {
      const { scrollTop, scrollHeight, clientHeight } = listinnerref.current;
      if (scrollTop + clientHeight === scrollHeight) {
        setCurrPage(currPage + 1);
      }
    }
  };

  useEffect(() => {
    const fetchData = async () => {
      await fetch(
        `http://dev3.dansmultipro.co.id/api/recruitment/positions.json${param}&page=${currPage}`
      )
        .then((response) => response.json())
        .then((actualData) => {
          var filtered = actualData.filter(function (el) {
            return el !== null;
          });
          if (!filtered.length) {
            setLastList(true);
            return;
          }
          setPrevPage(currPage);
          setData(filtered);
          setCountResult(filtered.length);
        })
        .catch((err) => {
          console.log(err.message);
        });
    };
    if (!lastList && prevPage !== currPage) {
      fetchData();
    }
    fetchData();
  }, [currPage, lastList, prevPage, param, listinnerref, countResult]);

  const handleChange = (event) => {
    if (event.target.name === "desc") {
      setDescription(event.target.value);
    }
    if (event.target.name === "loc") {
      setLocation(event.target.value);
    }
    if (event.target.name === "full-time" && event.target.checked === true) {
      setFullTIme(true);
    } else {
      setFullTIme(false);
    }
  };

  const onSearch = () => {
    setTitle(`Showing ${countResult} Jobs`);
    setParam(
      `?description=${description}&location=${location}&full_time=${fullTime}`
    );
  };

  return (
    <div className="p-5">
      <section className="pt-5">
        <div className="grid grid-cols-8 gap-1">
          <div className=" col-span-3">
            <label className="label">
              <span className="label-text">Job Description</span>
            </label>
            <input
              type="text"
              name="desc"
              placeholder="Filter by title, benefits, companies, experties"
              className="input input-bordered w-full max-w-xs"
              onChange={handleChange}
            />
          </div>
          <div className="w-full col-span-3">
            <label className="label">
              <span className="label-text">Job Description</span>
            </label>
            <input
              type="text"
              name="loc"
              placeholder="Filter by title, benefits, companies, experties"
              className="input input-bordered w-full max-w-xs"
              onChange={handleChange}
            />
          </div>
          <div className="form-control">
            <div className="m-auto">
              <label className="label cursor-pointer">
                <input
                  type="checkbox"
                  onChange={handleChange}
                  name="full-time"
                  className="checkbox"
                />
                <span className="label-text">Full Time Only</span>
              </label>
            </div>
          </div>
          <div className="form-control w-full max-w-xs">
            <div className="m-auto">
              <button className="btn" onClick={onSearch}>
                Search
              </button>
            </div>
          </div>
        </div>
      </section>
      <section className="pt-5">
        <JobListComponent
          listinnerref={listinnerref}
          data={data}
          title={title}
        />
        <div>
          <div className="grid grid-cols-1 gap-4 place-content-center h-48">
            <button
              className="btn bg-blue-700 flex text-center"
              onClick={onMore}
            >
              More Jobs
            </button>
          </div>
        </div>
      </section>
    </div>
  );
}
