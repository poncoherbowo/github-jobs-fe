import { useState } from "react";
import './App.css';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'
import Header from './components/Header'
import JobList from './pages/JobList'
import DetailJob from "./pages/DetailJob";
import { GoogleLogin, GoogleLogout } from 'react-google-login';
import { refreshTokenSetup } from './utils/refreshToken';
import { gapi } from "gapi-script";
const clientId =
  '502181811772-qeim3ejdnrf3oa1miopcjjra8mgsnmbr.apps.googleusercontent.com';

function App() {
  const [isLogin, setIsLogin] = useState(false);

  gapi.load("client:auth2", () => {
    gapi.client.init({
      clientId:
        clientId,
      plugin_name: "chat",
    });
  });

  const onSuccess = (res) => {
    setIsLogin(true)
    refreshTokenSetup(res);
  };

  const logout = () => {
    setIsLogin(false);
  };

  const onFailure = (res) => {
    console.log('Login failed: res:', res);
  };

  return (
    <div className="App">
      <Header />


      {!isLogin && (
        <div className="pt-10">
          <div className="grid grid-cols-6 gap-4">
            <div className="col-start-2 col-span-4">
              <GoogleLogin
                clientId={clientId}
                buttonText="Login with Google"
                onSuccess={onSuccess}
                onFailure={onFailure}
                cookiePolicy={'single_host_origin'}
                className="btn btn-block bg-transparent text-black btn-lg"
                isSignedIn={true}
              />
            </div>
          </div>
        </div>
      )}
      {isLogin && (
        <>
          <Router>
            <Routes>
              <Route exact path="/joblist" element={<JobList />} />
              <Route exact path="joblist/detail/:id" element={<DetailJob />} />
            </Routes>
          </Router>
          {/* <div>
            <div className="grid grid-cols-1 gap-4 place-content-center h-48">
              <GoogleLogout
                clientId={clientId}
                buttonText="Logout"
                onLogoutSuccess={logout}
                className="btn btn-block bg-transparent text-black btn-lg"
              />
            </div>
          </div> */}

        </>
      )}
    </div >
  );
}

export default App;
