import { Link } from "react-router-dom";
export default function JobListComponent({
  onScroll,
  listinnerref,
  data,
  title,
}) {
  return (
    <div
      className="box-content w-full border-4"
      ref={listinnerref}
      style={{ height: "100vh", overflowY: "scroll" }}
    >
      <div>
        <h1 className="text-4xl font-bold pl-3 pt-3">{title}</h1>
      </div>
      <div className="divider p-3"></div>
      {data.map((item, index) =>
        item != null ? (
          <div className="grid grid-cols-2" key={index}>
            <Link to={`detail/${item.id}`}>
              <span className="label-text text-2xl font-bold text-blue-600 p-4 col-span-1 ">
                {item.title ? item.title : ""}
              </span>
            </Link>
            <span className="label-text font-bold text-xl text-gray-400 p-4 text-end">
              {item.location}
            </span>
            <span className="label-text text-xl font-light text-gray-400 p-4">
              {item.company} -{" "}
              <span className="text-green-300 font-bold"> {item.type}</span>
            </span>
            <span className="label-text font-light text-xl text-gray-400 p-4 text-end">
              {item.created_at}
            </span>
            <div className="divider col-span-2 p-3"></div>
          </div>
        ) : (
          <div></div>
        )
      )}
    </div>
  );
}
