export default function Header() {
  return (
    <div className="navbar bg-blue-600">
      <h1 className="btn btn-ghost normal-case text-5xl text-base-100">GitHub Jobs</h1>
    </div>
  );
}
